<?php
	$dbconnstring = 'mysql:dbname=prova_imdb;host=localhost:3306';
	$dbuser = 'root';
	$dbpasswd = '';
	$db = null;

	try {
		$db = $GLOBALS['db'] = new PDO($dbconnstring, $dbuser, $dbpasswd);
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		echo 'error pdo failed';
		exit;
	}

	if(!isset($_GET['first_name']) || !isset($_GET['last_name']) || !isset($_GET['all'])){
		echo("error variables not set");
		exit;
	}

	$first_name = $db->quote($_GET['first_name']."%");
	$last_name = $db->quote($_GET['last_name']);
	$result = null;
	$actorId = getActorID($first_name, $last_name);

	if($_GET['all'] == "true"){
		#Query to get all movies where actor acts
		$result = $db->query("SELECT m.name, m.id, m.year
		FROM roles r
		JOIN movies m ON m.id = r.movie_id
		WHERE r.actor_id = $actorId
		ORDER BY m.year;");
	}else if($_GET['all'] == "false"){
		#Query to get movies where both actor and Kevin Bacon act
		$sqlQuery = 
			"SELECT m.name, m.id, m.year
			FROM roles r
			JOIN movies m ON m.id = r.movie_id
			WHERE r.actor_id = $actorId
			AND EXISTS(
				SELECT * 
				FROM actors k 
				JOIN roles rk ON k.id=rk.actor_id
				WHERE k.first_name='Kevin' AND k.last_name='Bacon'
				AND rk.movie_id=r.movie_id
			)
			ORDER BY m.year DESC, m.name ASC;"
		;

		$result = $db->query($sqlQuery);
	}else{
		echo 'false';
		exit;
	}

	if(!$result || $result->rowCount() <= 0){
		notFound();
	}

	$movies_array = [];
	
	#We convert the pdo query result to an array of associative arrays
	while($row = $result->fetchAll(PDO::FETCH_ASSOC)){
		$movies_array[] = $row;
	}
 	echo json_encode($movies_array);
	exit;

	function getActorId($first_name, $last_name){
		/** 
		 * Query to get the best actor choise for the given name, we choose it
		 * as the actor with higher film count
		*/
		$db = $GLOBALS['db'];

		$db->beginTransaction();
			$db->query("DROP VIEW IF EXISTS actors_with_count_films;");
			$db->query(
				"CREATE VIEW actors_with_count_films AS (
					SELECT a1.id AS id, a1.first_name as first_name, COUNT(m1.id) as count_films
					FROM actors a1
					JOIN roles r1 on a1.id=r1.actor_id
					JOIN movies m1 ON m1.id=r1.movie_id
					WHERE a1.first_name LIKE $first_name AND a1.last_name=$last_name
					GROUP BY (a1.id)
				);"
			);
			$queryToGetId =
			"SELECT a1.id FROM actors_with_count_films a1
			WHERE a1.count_films = 
				(SELECT MAX(a.count_films) AS count_films FROM actors_with_count_films a)
			ORDER BY a1.id DESC
			LIMIT 1;"; 
			$resultId = $db->query($queryToGetId);
			$db->query("DROP VIEW actors_with_count_films;");
		$db->commit();

		if(!$resultId || $resultId -> rowCount() < 0){
			echo("error");
			exit;
		}else if($resultId -> rowCount() == 0){
			//return that there is no such person in the database
			notFound();
		}
		$queryAsArray = $resultId->fetchAll();
		$actorId = $queryAsArray[0]['id'];
		return $actorId;
	}

	function notFound(){
		echo "false";
		exit();
	}
?>