$(function(){
	resetSearch();

	setSearchQuery("searchall");
	setSearchQuery("searchkevin");
});

function setSearchQuery(searchType){
	/**
	 * This function sets the Ajax response for the two buttons
	 */
	let search = searchType;
	let isAll;

	if(searchType === "searchall"){
		isAll = "true";
	}else if (searchType === "searchkevin"){
		isAll = "false";
	}else{
		showError("Invalid search");
		return;
	}

	$("#" + searchType + " input[name='submit']").click(function(){
		resetSearch();
		let firstName = $('#'+ searchType + ' input[name="firstname"]').val();
		let lastName = $('#'+ searchType + ' input[name="lastname"]').val();
		$("#firstN").text(firstName);
		$("#lastN").text(lastName);
		$.ajax({
			url: "../php/getMovieList.php",
			type: "GET",
			datatype: "json",
			data:{
				all: isAll,
				first_name: firstName,
				last_name: lastName,
			},
			success: getAllFilms,
			error: function(){showError("JSON request failed")}
			}
		)
	});
}

function getAllFilms(jsonFilms){
	/**
	 * This function displays the result of the Ajax request
	 */
	let films = null;
	try{
		films = JSON.parse(jsonFilms);
	}catch(e){
		/**When something goes wrong in getMovieList we echo an error string,
		 * that string throws an error when we call JSON.parse, so we display it
		 * in the console
		  */
		showError(jsonFilms);
		return;
	}
	if(!films){
		resultNotFound();
		return;
	}else{
		films[0].forEach(function(item){
			let tr = $('<tr></tr>');
			tr.html(
				"<td>"+item.id+"</td>"+
				"<td>"+item.name+"</td>"+
				"<td>"+item.year+"</td>"
			);
			$("#list tbody").append(tr);
		});
		showResults();
	}
}

function resetSearch(){
	/**This function clears the results and error errMsg divs */
	$("h2").hide();
	$("#list").hide();
	$('#errMsg').hide();
	$("firstN").empty();
	$("lastN").empty();
	$("errMsg").empty();
	$("#list tbody").empty();
	$("#errMsg").removeClass("error");
}

function showResults(){
	$("#list").show();
	$("#h2").show();
	$("#firstN").show();
	$("#lastN").show();
}

function resultNotFound(){
	$("#errMsg").show().text("No result found");
	$("h2").show();
}

function showError(error){
	$("#errMsg").addClass("error").show().text("An unexpected error occured, please try again later");
	console.log(error);
}