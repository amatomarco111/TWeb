"use strict";

(function(){
	const SHUFFLE_LEVEL = 3; // you con make the shuffle more "schuffled" by incresing this number
	var emptyTile = [3,3]; // [y-axis, x-axis]
	var shuffling = false; 

	window.onload = function(){
		let tiles = document.querySelectorAll("div#puzzlearea div");
		tiles.forEach( function(tile) {
			tile.onmouseover = highlightTile;
			tile.onmouseout = removeHighlightTile;
			tile.onclick = move;
		} );

		let shufflebutton = document.getElementById("shufflebutton");
		shufflebutton.onclick = shuffle;

		let victoryButton = document.getElementById("victory-button");
		victoryButton.onclick = hideVictory;
	};

	function highlightTile(){
		if(isMovable(this)){
			this.className += " highlight";
		}
	}

	function removeHighlightTile(){
		if(hasClass(this, "highlight")){
			this.classList.remove("highlight");
		}
	}

	function move(){
		moveTile(this);
	}

	function moveTile(tile){
		if(isMovable(tile)){
			swapTile(tile);
		}
	}
	
	function swapTile(movingTile){
		let emptyTilePositionClass = getClassNameFromPosition(emptyTile);
		let movingTilePositionClass = getClassNameFromPosition(getPosition(movingTile));
		emptyTile = getPosition(movingTile);
		movingTile.classList.remove(movingTilePositionClass);
		movingTile.classList.add(emptyTilePositionClass);
		if(!shuffling && checkIfComplete()){
			showVictory();
		}
	}

	function checkIfComplete(){
		for(let row=0; row<=3; row++){
			for(let column=0; column<=3; column++){
				let tilePositionIdentifier = "tile_"+row+"_"+column;
				let tileImageIdentifier = "image_"+row+"_"+column;
				let tileWithId = document.getElementById(tileImageIdentifier);
				let tileWithClass = document.getElementsByClassName(tilePositionIdentifier);

				if(tileWithClass.length>1){
					console.log("Error, found " + tileWithClass.length + " with same class");
					return false;
				}else if(tileWithClass[0] != tileWithId){
					return false;
				}
			}
		}
		console.log("true");
		return true;
	}

	function showVictory(){
		let victory = document.getElementById("victory");
		victory.classList.remove("hidden");
		victory.classList.add("shown");
	}

	function hideVictory(){
		let victory = document.getElementById("victory");
		victory.classList.add("hidden");
		victory.classList.remove("shown");
	}

	function shuffle(){
		shuffling = true;
		let numShuffles = SHUFFLE_LEVEL*50+(Math.floor(Math.random()*SHUFFLE_LEVEL*50));

		while(numShuffles){
			let possibleMoves = getPossibleMoves();
			let randomMovement = possibleMoves[Math.floor(Math.random()*possibleMoves.length)];
			let position = [...emptyTile];
			switch(randomMovement){
				case "up":
					position[0]--;
					break;
				case "right":
					position[1]++;
					break;
				case "down":
					position[0]++;
					break;
				case "left":
					position[1]--;
					break;
			}
			let className = getClassNameFromPosition(position);
			let tileToMoveArray = document.getElementsByClassName(className);
			let tileToMove = tileToMoveArray[0];
			moveTile(tileToMove);
			numShuffles--;
		}
		shuffling = false;
	}

	function getPossibleMoves(){
		let moves = [];
		if(emptyTile[0] > 0){
			moves.push("up")
		}
		if(emptyTile[1] > 0){
			moves.push("left")
		}
		if(emptyTile[0] < 3){
			moves.push("down")
		}
		if(emptyTile[1] < 3){
			moves.push("right")
		}
		return moves;
	}

	function getClassNameFromPosition(position){
		return("tile_" + position[0] + "_" + position[1]);
	}

	function isMovable(tile){
		let positionCoordinates = getPosition(tile);
		if(tilesNear(positionCoordinates, emptyTile)){
			return true;
		}else{
			return false;
		}
	}

	function tilesNear(a, b){
		if( ((a[0] === b[0]+1 || a[0] === b[0]-1) && a[1] === b[1] ) || //same column, adjacent line
			 (a[1] === b[1]+1 || a[1] === b[1]-1) && a[0] === b[0] )	//same line, adjacent column
		{
			return true;
		}
		else return false;
	}

	function getPositionClass(tile){
		/* returns an array that indicates the position of the tile.
		** the format is [y-value, x-value] */
		let className = tile.className;
		let startTilePositionPoint = className.search("tile");
		let positionClass = className.substring(startTilePositionPoint, startTilePositionPoint+8);
		return positionClass;
	}

	function getPosition(tile){
		/* returns an array that indicates the position of the tile.
		** the format is [y-value, x-value] */
		let positionClass = getPositionClass(tile);
		let position = positionClass.substring(5);
		let positionCoordinates = [parseInt(position.charAt(0)), parseInt(position.charAt(2))];
		return positionCoordinates;
	}

	function hasClass(element, className) {
		return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
	}
})();
