<?php
    /**
     * Account registrati:
     * email: amatomarco111@gmail.com   password:testtest
     * email: d@a.i                     password:testtest
     * email: r@o.i                     password:test1234
     * 
     */

    $PATH_TO_REDIRECT = "../html/complete/index.html";

    if(!isset($_SERVER["REQUEST_METHOD"]) || $_SERVER["REQUEST_METHOD"]!="POST"){
        exit("ERROR 400: Invalid request - This service accepts only POST requests.");
    }

    include './common.php';

    session_start();
    if(isset($_SESSION['email'])){
        echo 'already logged';
        exit;
    }

    try{
        $db = dbconnect();
    }catch(PDOException $e){
        echo "server down";
        exit;
    }

    if(!isset($_POST['email']) || !isset($_POST['password'])){
        echo 'variables_missing';
        exit;
    }

    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        echo'bad_email_format';
        exit;
    }

    $email = $db->quote($_POST['email']);
    $password = $db->quote($_POST['password']);
    
    $select_user_from_password = "
    SELECT * 
    FROM utenti 
    WHERE utenti.email = $email";

    $result = null;
    try{
        $result = $db->query($select_user_from_password);
    }catch(Exception $e){
        echo "query failed";
        exit;
    }

    $user = $result->fetch(PDO::FETCH_ASSOC);

    if(!$user){
        echo "user_not_found";
        exit;
    }else{
        if(!password_verify($password, $user['password'])){
            echo "wrong_password";
            exit;
        }
    }
    $_SESSION['email'] = $user['email'];

    $userToAssArray = array("email"=>$user['email']);

    $userJSON = json_encode($userToAssArray);

    echo $userJSON;
    exit;
?>