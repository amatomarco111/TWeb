-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 26, 2021 alle 22:33
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamehub`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `annunci`
--

CREATE TABLE `annunci` (
  `id` int(11) NOT NULL,
  `email_utente` varchar(200) NOT NULL,
  `piattaforma` varchar(50) NOT NULL,
  `titolo` varchar(100) NOT NULL,
  `testo` varchar(500) DEFAULT NULL,
  `path_immagine` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `annunci`
--

INSERT INTO `annunci` (`id`, `email_utente`, `piattaforma`, `titolo`, `testo`, `path_immagine`) VALUES
(11, 'r@o.i', 'NintendoSwitch', 'Zelda breath of the wild', 'The Legend of Zelda: Breath of the Wild è un videogioco action-adventure del 2017, sviluppato da Nintendo EPD e pubblicato da Nintendo per Nintendo Switch e Wii U. Considerato come un capolavoro della storia videoludica, si tratta del diciottesimo capitolo della celebre saga ', '6010841c637e09.03910157.jpg'),
(12, 'r@o.i', 'NintendoSwitch', 'Super Mario Odissey', 'IL VIAGGIO DI MARIO\r\nMario approda su Nintendo Switch con un\'avventura 3D senza precedenti! Questo fantastico gioco in stile sandbox - il primo dai tempi degli amatissimi Super Mario 64 del 1996 e Super Mario Sunshine del 2002 - è pieno zeppo di segreti e sorprese!', '601084a5c7a550.11221108.jpg'),
(13, 'r@o.i', 'NintendoSwitch', 'Super Mario 3D All stars', 'TRE GIOCHI PER UNA COLLEZIONE STELLARE!\r\nScopri tre delle più grandi avventure 3D di Mario con Super Mario 3D All-Stars per Nintendo Switch!', '6010854153e1e8.21055151.jpg'),
(14, 'r@o.i', 'Nintendo3DS', 'Gioco senza immagine', 'testo prova teto testo jof ew kfwao', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `piattaforme`
--

CREATE TABLE `piattaforme` (
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `piattaforme`
--

INSERT INTO `piattaforme` (`nome`) VALUES
('Nintendo3DS'),
('NintendoSwitch'),
('PS5'),
('XboxSeriesX');

-- --------------------------------------------------------

--
-- Struttura della tabella `preferiti`
--

CREATE TABLE `preferiti` (
  `id_annuncio` int(11) NOT NULL,
  `email_utente` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `preferiti`
--

INSERT INTO `preferiti` (`id_annuncio`, `email_utente`) VALUES
(12, 'amatomarco111@gmail.com'),
(13, 'amatomarco111@gmail.com');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `data_di_nascita` date NOT NULL,
  `genere` enum('M','F') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`email`, `password`, `data_di_nascita`, `genere`) VALUES
('amatomarco111@gmail.com', '$2y$10$2o5ta8o6VNwOavF78XH7aetvx91wmOmEEWHwazaP.nv1tUdzaDnhy', '1999-04-22', 'M'),
('d@a.i', '$2y$10$BctJ3upjJBQuFTLjFakZiuiYsUWNnu8TGBCQDBoT9/bgRzcSrEpU2', '1948-05-01', 'F'),
('r@o.i', '$2y$10$1hUnCSMgDHVzF5ITa5eUeeaOUBGZ0eXFut7L1/RxTntTu0XuV9zay', '2001-02-01', 'F');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `annunci`
--
ALTER TABLE `annunci`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_utente` (`email_utente`),
  ADD KEY `piattaforma` (`piattaforma`);

--
-- Indici per le tabelle `piattaforme`
--
ALTER TABLE `piattaforme`
  ADD PRIMARY KEY (`nome`);

--
-- Indici per le tabelle `preferiti`
--
ALTER TABLE `preferiti`
  ADD PRIMARY KEY (`id_annuncio`,`email_utente`),
  ADD KEY `email_utente` (`email_utente`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `annunci`
--
ALTER TABLE `annunci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `annunci`
--
ALTER TABLE `annunci`
  ADD CONSTRAINT `annunci_ibfk_1` FOREIGN KEY (`email_utente`) REFERENCES `utenti` (`email`),
  ADD CONSTRAINT `annunci_ibfk_2` FOREIGN KEY (`piattaforma`) REFERENCES `piattaforme` (`nome`);

--
-- Limiti per la tabella `preferiti`
--
ALTER TABLE `preferiti`
  ADD CONSTRAINT `preferiti_ibfk_1` FOREIGN KEY (`id_annuncio`) REFERENCES `annunci` (`id`),
  ADD CONSTRAINT `preferiti_ibfk_2` FOREIGN KEY (`email_utente`) REFERENCES `utenti` (`email`),
  ADD CONSTRAINT `preferiti_ibfk_3` FOREIGN KEY (`id_annuncio`) REFERENCES `annunci` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
