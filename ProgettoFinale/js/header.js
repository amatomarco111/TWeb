const loginErrorMap = new Map();
loginErrorMap.set("server_down", "Il server non è raggiungibile, prova più tardi");
loginErrorMap.set("user_not_found", "L'email inserita non corrisponde a nessun account, riprova");
loginErrorMap.set("wrong_password", "Password errata");


$(function(){
    /**
     * Personal options are shown when user hovers the personal area
     * where the user email and user icon are shown
    */
    $('#user-menu').mouseover(function(){
        $('.user-options').removeClass('hide');
    });
    
    /**
     * Personal options are hidden when user mouse leaves the personal area
     * where the user email and user icon are shown
    */
    $('#personal-area').mouseleave(function() {
        $('#user-options-container div').addClass('hide');
    });


    /**
     * When user clicks on "Accedi" button submitLogin is called
     */
    $("#login-form").submit(function(e){
        e.preventDefault();
        submitLogin();
    });

    /**
     * When user clicks on logout an AJAX requests logout and then
     * showLoginForm is called
     */
    $("#logout").click(function(e){
        e.preventDefault();
        $.post({
            url: "../../php/logout.php",
            datatype: "text",
            success: function(logoutResult){
                if(logoutResult === "okay"){
                    showLoginForm();
                }
            }
        })
    });


    /**
     * On page loading this function shows either the login or the personal bar
     */
    $.post({
        url: "../../php/autentication.php",
        datatype: "text",
        success: showLoginFormOrPersonalArea
    });
});

/**
 * If response is not null it contains the user email, so we show the personal bar,
 * otherwise uses has not logged and login form is shown
 * 
 * @param {string} response is a not empty string when user is logged, so the personal area is shown
 */
function showLoginFormOrPersonalArea(response){
    if(!response){
        showLoginForm();
    }else{
        showPersonalArea(response);
    }
}

/**
 * Login form is submitted to login.php, output is processed by processLoginResult()
 */
function submitLogin(){
    let email = $("#email").val();
    let password = $("#password").val();
    $.post({
        url: "../../php/login.php",
        data: {
            'email': email,
            'password': password
        },
        datatype: "json",
        success: processLoginResult
    });
}

/**
 * Shows an error message if loginResult is string, otherwise gets email from JSON object and shows personal area with email
 * 
 * @param {*} loginResult If not JSON then is the string containing error, if JSON it is a JSON containing email value in 'email' key.
 */
function processLoginResult(loginResult){
    let loginResultJSON = null;
    try{
        loginResultJSON = JSON.parse(loginResult);
    }catch(e){
        showLoginError(loginResult);
        return;
    }
    showPersonalArea(loginResultJSON.email);
}

/**
 * Calls fillError passing the right parameters
 * 
 * @param {string} errorType The error type
 */
function showLoginError(errorType){
    console.log("Login error: " + errorType);
    let errorElement = $("#login-div p.error");
    let defaultErrorText = "Si è verificato un errore, riprova più tardi";
    fillError(errorElement, loginErrorMap, errorType, defaultErrorText);
}

/**
 * Hides personal area, shows login form
 */
function showLoginForm(){
    $("#personal-area").hide();
    $("#login-div").show();
}

/**
 * Hides login form, shows personal area showing email in personal area
 * 
 * @param {string} email user email to show in personal area 
 */
function showPersonalArea(email){
    $("#login-div").hide();
    $("#personal-area").show();
    $("#user-menu span").text(email);
}