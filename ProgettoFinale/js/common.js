$(function(){
    let selectPiattaforme = $("select#piattaforma");
    if(selectPiattaforme.length){ //We have a piattaforme select to fill
        $.post({
            url: "../../php/get-piattaforme.php",
            datatype: "json",
            success: function(data){
                fillSelectPiattaforme(selectPiattaforme, data);
            }
        });
    }
});

/**
 * The url is parsed to return the value corresponding to sParam
 * 
 * @returns sParam value if set, otherwhise null
 * @param {string} sParam The key of the GET parameter we want to retrieve 
 */
function getURLParameter(sParam)
{
    let sPageURL = window.location.search.substring(1); //window.location.search returns url parameters including the first '?', so substring ignores it
    let sURLVariables = sPageURL.split('&');
    for (let i = 0; i < sURLVariables.length; i++) 
    {
        let sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
    return null;
}

/**
 * Fills selectPiattaforme with piattaforme options if the php processed correctly
 * 
 * @param {*} selectPiattaforme The select element that has to be filled by piattaforme
 * @param {*} piattaformeJson The result of the php to get all the piattaforme registered. It is a string in case of error,
 * Otherwhise it is a JSON representing the piattaforme[]
 */
function fillSelectPiattaforme(selectPiattaforme, piattaformeJson){
    let piattaformeArray = null;
    try{
        piattaformeArray = JSON.parse(piattaformeJson);
    }catch(e){
        console.log("errore caricamento piattaforme: "+piattaformeJson);
        return;
    }
    
    for(piattaforma of piattaformeArray){
        let nome = (piattaforma.nome).replace(/_/g, " ");
        let piattaformaHTML = '<option value="'+ piattaforma.nome +'">'+ nome +'</option>';
        selectPiattaforme.append(piattaformaHTML);
    }
}

/**
 * If error is a key of errorsNameTextMap the corrisponding value is inserted in errorElement, otherwise
 * defaultErrorText is inserted in errorElement
 * 
 * @param {*} errorElement The HTML element where the error will be inserted
 * @param {Map<string, string>} errorsNameTextMap The map that contains the error types as keys with
 * the corrisponding message to be shown to the user as value
 * @param {*} error The error type
 * @param {*} defaultErrorText The message to be shown in errorElement if error is not a key of errorsNameTextMap
 */
function fillError(errorElement, errorsNameTextMap, error, defaultErrorText){
    for(const[key, value] of errorsNameTextMap.entries()){
        if(key === error){
            errorElement.text(value);
            return;
        }
    }
    errorElement.text(defaultErrorText);
}

/**
 * This function calls fillError passing the #main div as errorElement
 * 
 * @param {*} errorsNameTextMap the errorsNameTextMap to be passed
 * @param {*} error the error to be passed
 * @param {*} defaultErrorText the defaultErrorText to be passed
 */
function displayError(errorsNameTextMap, error, defaultErrorText){
    $("#main").empty();
    fillError($("#main"), errorsNameTextMap, error, defaultErrorText);
}