const searchErrorMap = new Map();
searchErrorMap.set("server_down", "Il server non è raggiungibile, riprovare più tardi");
searchErrorMap.set("query_failed", "Non è possibile visualizzare gli annunci, riprovare più tardi");

const idAnnunci = [];

$(function () {
    let personal = getURLParameter("personal");
    let preferiti = getURLParameter("preferiti");
    let nome = getURLParameter("nome");
    let piattaforma = getURLParameter("piattaforma");

    if (personal === "true") {
        //User clicked on "I tuoi annunci" in personal div

        $("#advertisements").before("<h1> I tuoi annunci </h1>");

        $.post({
            url: "../../php/autentication.php",
            datatype: "text",
            success: showPersonalSearch,
            error: function () {
                showSearch("server_unreachable");
            }
        });

        return;
    }

    if(preferiti === "true"){
        //User click on "Annunci preferiti" in personal div

        $("#advertisements").before("<h1> I tuoi annunci preferiti</h1>");

        $.post({
            url: "../../php/autentication.php",
            datatype: "text",
            success: showPreferitiSearch,
            error: function () {
                showSearch("server_unreachable");
            }
        });

        return;
    }

    if (!nome || !piattaforma) {
        //This should not happen, error is shown
        showSearch("variables_not_set");
        return;
    }

    if (piattaforma === "all") {
        //User is looking for games with name 'nome' for any platforms

        $("#advertisements").append('<h1> ' + nome + ' - tutte le piattaforme </h1>');
    } else {
        //User is looking for games with name 'nome' for platform 'piattaforma'

        $("#advertisements").append('<h1> ' + nome + ' - ' + piattaforma + ' </h1>');
    }

    //On success shows search, otherwise shows corresponding error
    $.post({
        url: "../../php/search.php",
        datatype: "json",
        data: {
            'nome': nome,
            'piattaforma': piattaforma
        },
        success: showSearch,
        error: function () {
            showSearch("server_unreachable");
        }
    });
});


/**
 * If autentication failed it redirects to index, otherwise shows search containing annunci published by user.
 * 
 * @param {string} autentication user email if autentication succeded, null otherwise
 */
function showPersonalSearch(autentication) {
    if (autentication === null) {
        window.location.replace("./index.shtml");
    } else {
        $.post({
            url: "../../php/search.php",
            datatype: "json",
            data: {
                'personal': 'true'
            },
            success: showSearch,
            error: function () {
                showSearch("server_unreachable");
            }
        });
    }
}

/**
 * If autentication failed it redirects to index, otherwise shows search containing annunci preferiti by user.
 * 
 * @param {string} autentication user email if autentication succeded, null otherwise
 */
function showPreferitiSearch(autentication) {
    if (autentication === null) {
        window.location.replace("./index.shtml");
    } else {
        $.post({
            url: "../../php/search.php",
            datatype: "json",
            data: {
                'preferiti': 'true'
            },
            success: showSearch,
            error: function () {
                showSearch("server_unreachable");
            }
        });
    }
}

/**
 * Shows error message calling fillError() if data is string, otherwise composes annunci HTML and 
 * shows them
 * 
 * @param {*} data contains string if error occured in back-end, otherwise JSON containing data of annunci
 */
function showSearch(data) {
    $("#advertisements").html("");

    let jsonAnnunci = null;
    try {
        jsonAnnunci = JSON.parse(data);
    } catch (e) {
        console.log("error: " + data);
        let searchErrorElement = $("#search-error");
        let defaultErrorText = "Si è verificato un errore, riprova più tardi";
        fillError(searchErrorElement, searchErrorMap, data, defaultErrorText);
        return;
    }

    let indexAnnuncio = 0;

    let piattaforma = getURLParameter("piattaforma");
    if(piattaforma === "all"){
        piattaforma = "Tutte le piattaforme";
    }
    let nome_raw;
    if(nome_raw = getURLParameter("nome")){
        //Gets the nome and converts underscores with spaces
        const nome = nome_raw.replace(/_/g, " ");
        let h1 = "<h1> "+ nome +" - "+ piattaforma +" </h1>";
        $("#advertisements").append(h1);
    }

    for (annuncio of jsonAnnunci) {
        let divAdvert = '<div class="advert" id="annuncio' + indexAnnuncio + '">';
        if (annuncio.path_immagine !== null) {
            let imgAdvert = '<img src="../../img/advert-img/' + annuncio.path_immagine + '" alt="' + annuncio.titolo + '">';

            divAdvert += "<div class='advert-image'>";
                divAdvert += "<a href = 'advert.shtml?id="+annuncio.id+"'>";
                    divAdvert += imgAdvert;
                divAdvert += "</a>";
            divAdvert += "</div>";
        }
            divAdvert += "<div class='advert-text'>";
                divAdvert += "<a href = 'advert.shtml?id="+annuncio.id+"'>";
                    divAdvert += '<h2>' + annuncio.titolo + '</h2>';
                    divAdvert += '<p>' + annuncio.testo + '</p>';
                divAdvert += "</a>";
            divAdvert += "</div>";
            divAdvert += '<img src="../../img/icons/empty_heart.png" alt="" id=heart'+ indexAnnuncio +' class="heart">';
        divAdvert += '</div>';

        $("#advertisements").append(divAdvert);

        //composes idAnnunci array, which will be used in loadPreferiti
        idAnnunci.push(annuncio.id);
        indexAnnuncio++;
    }

    loadPreferiti();
}

 /**
  * Sends AJAX requesto to get annunci preferiti in the current search,
  * they are found into idAnnunci, which is a global variable.
  * Then calls updateSearchPreferiti() passing the php returned value.
  */
function loadPreferiti() {
    $.post({
        url: "../../php/getPreferiti.php",
        datatype: "json",
        data: {
            'id_annunci': idAnnunci
        },
        success: updateSearchPreferiti,
        error: function () {
            showSearch("server_unreachable");
        }
    });
}

/**
 * If arrayPreferiti[i] is true then the annuncio in #advert-i is altready preferito and
 * the full-heart is shown. Otherwise it is not preferito and empty-heart is shown
 * 
 * @param {*} arrayPreferiti boolean array containg true in 
 * arrayPreferiti[i] if the annuncio in idAnnunci[i] is already preferito
 * for the user, false otherwise. When an error occured it contains a string
 * of the error type
 */
function updateSearchPreferiti(arrayPreferiti) {
    let jsonPreferiti = null;
    try {
        jsonPreferiti = JSON.parse(arrayPreferiti);
    } catch (e) {
        if(arrayPreferiti !== "access_denied" && arrayPreferiti !== "id_annunci_missing"){
			console.log(arrayPreferiti);
		}
		return;
    }

    let numAnnuncio = 0;

    for (isPreferito of jsonPreferiti) {
        if(isPreferito){
            //fullHeart is shown, removePreferiti on click
            $("#annuncio"+numAnnuncio+" .heart").attr("src","../../img/icons/full_heart.png");
            $("#annuncio"+numAnnuncio+" .heart").click(removePreferiti);
        }else{
            //empty heart is shown, addPreferiti on click
            $("#annuncio"+numAnnuncio+" .heart").click(addPreferiti);
        }
        numAnnuncio++;
    }
}

/**
 * Sends AJAX request to remove annuncio from preferiti, if succesful
 * Empty heart is shown, otherwise error message is shown
 */
function removePreferiti(){
    const heartElement = $(this);
    const id_clicked = $(this).attr("id");
    const indexAnnuncio = id_clicked.slice(-1);
    
    $.post({
        url: "../../php/preferiti-operations.php",
        data: {
            operation: "remove",
            id_annuncio: idAnnunci[indexAnnuncio]
        },
        datatype: "text",
        success: function(returnValue){
            if(returnValue !== "success"){
                showPreferitiError($(this), returnValue);
                return;
            }
            heartElement.off();
            heartElement.click(addPreferiti);
            heartElement.attr("src","../../img/icons/empty_heart.png");
        },
        error: function(){
            showPreferitiError($(this),"page_not_found");
        }
    });
}

/**
 * Sends AJAX request to add annuncio in preferiti, if succesful
 * full heart is shown, otherwise error message is shown
 */
function addPreferiti(){
    const heartElement = $(this);
    const id_clicked = $(this).attr("id");
    const indexAnnuncio = id_clicked.slice(-1);
    
    $.post({
        url: "../../php/preferiti-operations.php",
        data: {
            operation: "add",
            id_annuncio: idAnnunci[indexAnnuncio]
        },
        datatype: "text",
        success: function(returnValue){
            if(returnValue !== "success"){
                showPreferitiError($(this), returnValue);
                return;
            }
            heartElement.off();
            heartElement.click(removePreferiti);
            heartElement.attr("src","../../img/icons/full_heart.png");
        },
        error: function(){
            showPreferitiError($(this),"page_not_found");
        }
    });
}

function showPreferitiError(heartElement, errorType){
    console.log(errorType);
}