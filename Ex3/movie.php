<!DOCTYPE html>
<html>
	<head>

		<!--
		-- Author: Marco Amato
		-- Lab1 - Esercizio 3
		-- Inserimento di elementi dinamici a partire del sito statico relativo all es. 2
		-->
		<title>Rancid Tomatoes</title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link href="movie.css" type="text/css" rel="stylesheet">
		<link rel="icon" type="image/png" href="https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif"/>
	</head>

	<?php
		//ottengo titolo, data di uscita e rating del film tramite la variabile passata in url
		$film = $_GET["film"];
		$info = file($film . "/info.txt", FILE_IGNORE_NEW_LINES);
		list($title, $release, $rating) = $info;
	?>

	<body>
		<div id="banner">
			<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
		</div>

		<h1><?= $title ?> (<?= $release ?>)</h1>

		<div id="main">

			<div id="info">
				
				<div>
					<img src="<?=$film?>/overview.png" alt="general overview">
				</div>

				<dl id="info-list">

				<?php
				//inserisco le informazioni dell'overview fornite dal file di testo...
				$overview = file($film . "/overview.txt", FILE_IGNORE_NEW_LINES);
				for($i=0; $i<count($overview); $i++){
					$overview_piece = explode(":",$overview[$i]); ?>
					<dt> <?= $overview_piece[0]; ?> </dt>
					<dd> <?= $overview_piece[1]; ?></dd>
				<?php
				}
				?>
				</dl>
			</div>

			<div id="reviews">
				<div id="reviews_banner">
					<img src=
						<?php
						//In base al rating associo l'immagine fresh o rotten...
						if(intval($rating) <= 60){
							echo('"https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rottenbig.png" alt="Rotten"');
						}
						else{
							echo('"https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/freshbig.png" alt="Rotten"');
						}
						?>
					>
					<span> <?= $rating; ?>% </span>
				</div>

				<?php
				//salvo gli indirizzi delle recensioni del film nella variabile $reviews_names
				$reviews_names = glob("$film/review*.txt");
				$num_reviews=count($reviews_names);
				?>

				<div class="column" id="column-left">
					<?php
					//Passo il contenuto della review alla funzione print_review.
					//Nella colonna sinistra stampo la prima metà delle review.
					for($index_reviews=0;$index_reviews< ($num_reviews/2); $index_reviews++ ){
						$review_content = file($reviews_names[$index_reviews], FILE_IGNORE_NEW_LINES);
						print_review($review_content);
					}
					?>
				</div>

				<div class="column" id="column-right">
					<?php
					//Nella colonna destra stampo le review restanti.
					for($index_reviews=$num_reviews/2+1; $index_reviews< $num_reviews; $index_reviews++ ){
						$review_content = file($reviews_names[$index_reviews], FILE_IGNORE_NEW_LINES);
						print_review($review_content);
					}
					?>
				</div>

			</div>

			<p id="page-number">(1-<?=$num_reviews?>) of <?=$num_reviews?></p>

		</div>

		<div id="validators">
			<a href="http://validator.w3.org/check/referer">
                <img width="88" src=
"https://upload.wikimedia.org/wikipedia/commons/b/bb/W3C_HTML5_certified.png "
		alt="Valid HTML5!">
            </a> <br>
			<a href="http://jigsaw.w3.org/css-validator/check/referer"><img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!"></a>
		</div>
	</body>

	<?php
		function print_review($review_content){
			//print_review prende in input l'array contenente le informazioni della review e le stampa. 
			list($review_quote, $review_vote, $review_author, $review_publication) = $review_content; ?>
			<div class="review">
				<div class="review-content">
					<p>
						<img src= <?php 
						//in base al voto fresh o rotten cambia immagine
						if($review_vote == "FRESH") {?>
							"https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/fresh.gif" alt="fresh">
						<?php }else if($review_vote == "ROTTEN"){ ?>
							"https://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" alt="rotten">
						<?php } ?>
						<q> <?=$review_quote?> </q>
					</p>
				</div>
				<div class="review-author">
					<p>
						<img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
						<?= $review_author ?> <br>
						<span class="publication"><?= $review_publication ?> </span>
					</p>
				</div>
			</div>
		<?php
		}
	?>

</html>