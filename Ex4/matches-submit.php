<?php readfile("top.html"); ?>
<?php
$inputUsers = file("./singles.txt", FILE_IGNORE_NEW_LINES);
$usersArray = process_users($inputUsers);
$userToMatch = find_user($usersArray, $_GET['name']);
$usersMatched = find_matches($userToMatch, $usersArray);
?>
<h1>Matches for <?=$userToMatch["name"]?></h1>
<?php
	foreach($usersMatched as $user){
?>	<div class="match">
		<p><img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/user.jpg" alt="user image">
		<?=$user['name'];?></p>
		<ul>
			<li>
				<strong>gender:</strong> <?=$user['gender']?>
			</li>
			<li>
				<strong>age:</strong> <?=$user['age']?>
			</li>
			<li>
				<strong>type:</strong> <?=$user['personality']?>
			</li>
			<li>
				<strong>OS:</strong> <?=$user['os']?>
			</li>
		</ul>
	</div>
<?php }
?>
<?php readfile("bottom.html"); ?>

<?php
	function process_users($rawInput){
		$usersArray = [];
		for($i=0; $i<count($rawInput); $i++){
			list($name, $gender, $sex_orientation, $age, $personality, $os, $minAge, $maxAge) = explode(",", $rawInput[$i]);
			$user['name'] = $name;
			$user['gender'] = $gender;
			$user['sex-orientation'] = $sex_orientation;
			$user['age'] = $age;
			$user['personality'] = $personality;
			$user['os'] = $os;
			$user['minAge'] = $minAge;
			$user['maxAge'] = $maxAge;

			$usersArray[$i] = $user;
		}
		return $usersArray;
	}

	function find_user($usersArray, $username){
		for($i=0; $i<count($usersArray); $i++){
			$user = $usersArray[$i];
			if($user['name'] == $username){
				array_splice($usersArray, $i, 1);
				return $user;
			}
		}
	}

	function find_matches($userToMatch, $usersArray){
		$usersMatched = [];
		foreach($usersArray as $user){
			if(
				( user_likes_user($user, $userToMatch) && user_likes_user($userToMatch, $user))
				&& ($userToMatch['minAge'] <= $user['age'] && $user['age'] <= $userToMatch['maxAge'])
				&& ($userToMatch['os'] == $user['os'])
				&& character_matches_position($userToMatch['personality'], $user['personality'])
			){
				array_push($usersMatched, $user);
			}
		}
		return $usersMatched;
	}

	function character_matches_position($str1, $str2){
		for($i = min(strlen($str1), strlen($str2))-1; $i>=0; $i--){
			if($str1[$i] == $str2[$i]) return true;
		}
		return false;
	}

	function user_likes_user($user1, $user2){
		if($user1['sex-orientation'] == "MW"){
			return true;
		}else if($user1['sex-orientation'] == $user2['gender']){			
			return true;
		}

		return false;
	}
?>