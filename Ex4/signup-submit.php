<?php readfile("top.html"); ?>
<?php

$name = $_POST['name'];
$gender = $_POST['gender']; 
$sex_orientation = get_sexual_orientation_string($_POST["sex-orientation"]);
$age = $_POST['age'];
$personality = $_POST['personality'];
$os = $_POST['os'];
$min= $_POST['min-age'];
$max = $_POST['max-age'];

$record_string = implode(',', array($name, $gender, $sex_orientation, $age, $personality, $os, $min, $max)) . "\n";

file_put_contents("singles.txt", $record_string, FILE_APPEND);

?>

<h1>Thank you!</h1>

<?php readfile("bottom.html"); ?>

<?php
    function get_sexual_orientation_string($array_of_sexes){
        if(count($array_of_sexes) == 2 && 
            ( ($array_of_sexes[0] == "women" && $array_of_sexes[1] == "men") 
            || ($array_of_sexes[0] == "men" && $array_of_sexes[1] == "women") ) 
        ){
            return "MW";
        }else{
            if($array_of_sexes[0] == "women") return "W";
            else if($array_of_sexes[0] == "men") return "M";
            else return "ERROR";
        }
    }
/*
    function get_multiple_values_from_post($value_of_variable){
        $array_of_values = [];
        if(!empty($_POST[$value_of_variable])){
            foreach($_POST[$value_of_variable] as $value){
                array_push($array_of_values, $value);
            }
        }
        return $array_of_values;
    }
*/
?>