<?php readfile("top.html"); ?>

<form action="signup-submit.php" method="post">
    <fieldset>
        <legend>New User Signup</legend>
        <label class="left" for="name">Name: </label>
        <input type="text" name="name" id="name" size="16">
        <br>
        
        <label class="left" for="male">Gender:</label>
        <input type="radio" name="gender" id="male" value="M">Male
        <input type="radio" name="gender" id="female" value="F" checked>Female
        <br>

        <label class="left" for="male">I am looking for:</label>
        <input type="checkbox" name="sex-orientation[]" id="men" value="men">Men
        <input type="checkbox" name="sex-orientation[]" id="women" value="women">Women
        <br>

        <label class="left" for="age">Age: </label>
        <input type="text" name="age" id="age" size="6" maxlength="2">
        <br>
        
        <label class="left" for="personality">Personality type: </label>
        <input type="text" name="personality" id="personality" size="6" maxlength="4"> (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
        <br>

        <label class="left" for="os">Favourite OS: </label>
        <select name="os" id="os">
            <option value="linux" selected>Linux</option>
            <option value="mac-os">Mac OS</option>
            <option value="Windows">windows</option>
        </select>
        <br>

        <label class="left" for="seek-age">Seeking age: </label>
        <input type="text" name="min-age" id="min-age" size="6" maxlength="3" placeholder="min age"> to
        <input type="text" name="max-age" id="max-age" size="6" maxlength="3" placeholder="max age">
        <br>

        <input type="submit" value="Sign up">
    </fieldset>
</form>

<?php readfile("bottom.html"); ?>